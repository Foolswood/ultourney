#!/usr/bin/env python

from random import Random
from collections import defaultdict


class Team:
    def __init__(self, name):
        self.name = name
        self.players = []

    def __len__(self):
        return len(self.players)

    def __bool__(self):
        return bool(self.players)

    def __repr__(self):
        return f'<{type(self).__name__} {self.name} {self.players}>'

    def count(self, criterion, value):
        i = 0
        for p in self.players:
            if p[criterion] == value:
                i += 1
        return i

    def total(self, criterion):
        return sum(p[criterion] for p in self.players)

    def add(self, player):
        self.players.append(player)


def cyclic_permutations(iterable):
    '''Go through an iterable repeatedly cyclicly permuting the order'''
    while True:
        yield from iterable
        iterable.append(iterable.pop(0))


def assign_to_teams(weights, players, team_names):
    '''Sort players into teams

    weights is a dict mapping survey question to importance in sorting
    players is a list of dicts of players survey responses
    team_names are the names of the teams you want to split them into

    Basically how this works is that we cyclicly permute the teams in turn
    finding the player that brings the team they are put in as close to the
    "everyone" team as possible.

    There are 2 kinds of question response this knows how to deal with:
     - Numeric -> aims for the mean
     - String  -> aims to match the distribution of answers of the "everyone" team
    '''
    teams = [Team(n) for n in team_names]
    # Make a team that with everyone in it so we can use the averages from it
    # to make a target for the fitness function:
    avg_team = Team(None)
    for p in players:
        avg_team.add(p)

    def avg_dist(criterion, t, p):
        v = p[criterion]
        if isinstance(v, str):
            return ((t.count(criterion, v) + 1) / (len(t) + 1)) - (avg_team.count(criterion, v) / len(avg_team))
        else:
            return ((t.total(criterion) + v) / (len(t) + 1)) - (avg_team.total(criterion) / len(avg_team))
    for t in cyclic_permutations(teams):
        if not players:
            break
        def team_fit_function(p):
            r = 0
            for criterion, weight in weights.items():
                r += (avg_dist(criterion, t, p) ** 2) * weight
            return r
        # Take the players furthest from average in the first pass to allow the
        # algorithm time to adjust for them as we go:
        players.sort(key=team_fit_function, reverse=bool(t))
        # Pop takes from the end, so sort puts the one we want there.
        t.add(players.pop())
    return teams


def print_assignment_quality(weights, assignments):
    '''Print out some stuff to check the balance of teams'''
    avg_team = Team(None)
    options = defaultdict(set)
    for t in assignments:
        for p in t.players:
            avg_team.add(p)
            for criterion, value in p.items():
                if criterion not in weights:
                    continue
                if isinstance(value, str):
                    options[criterion].add(value)
                else:
                    options[criterion] = None
    example_p = avg_team.players[0]
    weighted_distance = {t.name: 0 for t in assignments}
    for criterion, values in options.items():
        if values is None:  # Numeric
            avg_d = avg_team.total(criterion) / len(avg_team)
            print(criterion, avg_d)
            for t in assignments:
                d = t.total(criterion) / len(t)
                weighted_distance[t.name] += weights[criterion] * ((d - avg_d) ** 2)
                print(' ', t.name, d)
        else:
            print(criterion)
            for o in values:
                avg_d = avg_team.count(criterion, o) / len(avg_team)
                print(' ', o, avg_d)
                for t in assignments:
                    d = t.count(criterion, o) / len(t)
                    weighted_distance[t.name] += weights[criterion] * ((d - avg_d) ** 2)
                    print('  ', t.name, d)
    print(weighted_distance)


def print_assignments(assignments):
    '''Print out who is in which team'''
    r = Random()
    r.seed(327)  # We don't actually care about being random, just not in an obvious order
    for t in assignments:
        print(t.name)
        r.shuffle(t.players)  # Shuffle with fixed seed to make it stable, but not saying anything
        for p in t.players:
            print('    ', p['name'])


# criteria_weights = {
#     'matches': 5,
#     'skill': 2,
#     'height': 1,
#     'position': 1
# }
# 
# 
# all_players = [
#     {'name': 'A', 'skill': 4, 'height': 'tall', 'matches': 'M', 'position': 'cutter'},
#     {'name': 'B', 'skill': 3, 'height': 'medium', 'matches': 'F', 'position': 'cutter'},
#     {'name': 'C', 'skill': 4, 'height': 'short', 'matches': 'F', 'position': 'handler'},
#     {'name': 'D', 'skill': 3, 'height': 'short', 'matches': 'M', 'position': 'handler'},
#     {'name': 'E', 'skill': 2, 'height': 'medium', 'matches': 'M', 'position': 'cutter'},
#     {'name': 'F', 'skill': 3, 'height': 'medium', 'matches': 'F', 'position': 'cutter'},
#     {'name': 'G', 'skill': 4, 'height': 'tall', 'matches': 'F', 'position': 'handler'},
#     {'name': 'H', 'skill': 3, 'height': 'medium', 'matches': 'F', 'position': 'cutter'},
#     {'name': 'I', 'skill': 4, 'height': 'short', 'matches': 'M', 'position': 'handler'},
#     {'name': 'J', 'skill': 5, 'height': 'tall', 'matches': 'M', 'position': 'handler'},
# ]
# 
# team_names = ['some', 'other']
# 
# a = assign_to_teams(criteria_weights, all_players, team_names)
# print_assignment_quality(criteria_weights, a)
# print_assignments(a)

def prepare(players):
    '''Transform a CSV row to end up with the fields and info you want for sorting'''
    r = []
    heights = ['Short ass', 'Average Height', 'Tall but closer to Average', 'Absolute Giant']
    for p in players:
        mp = {
            'position': p['What position do you prefer to play in?'],
            'height': heights.index(p['How tall are you?']),
            'throw': int(p['Throwing Accuracy/Ability']),
            'catch': int(p['Receiving Ability (running, jumping, catching)']),
            'name': p["What's your name?"],
            'match': p['Matching']}
        r.append(mp)
    return r

weights = {  # How much weight to give each criterion in balancing process
    'match': 5,
    'throw': 2,
    'catch': 2,
    'height': 1,
    'position': 1
}

from csv import DictReader
with open('players.csv') as f:
    mapped_players = prepare(DictReader(f))
    assignments = assign_to_teams(weights, mapped_players, ['Red', 'Green', 'Blue', 'Yellow'])
    # print_assignment_quality(weights, assignments)
    print_assignments(assignments)
