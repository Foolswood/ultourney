Algorithm for assigning hat tournament entrants to teams based on their
questionnaire answers.

Calling `assign_to_teams` with appropriate arguments will give you back a dict
of `Team` objects containing a list of players.

The rest is all internals and examples.
